/*Programa para Jupiter*/

#include <stdio.h>

int main() {
    int gas, helio, gravidade, hidrogenio, dinheiro;
    
    printf("Digite a quantidade de dinheiro: ");
    scanf("%d", &dinheiro);
    
    gas = dinheiro / 50;
    dinheiro -= gas * 50;
    gravidade = dinheiro / 10;
    dinheiro -= gravidade * 10;
    helio = dinheiro / 5;
    dinheiro -= helio * 5;
    hidrogenio = dinheiro;
    
    printf("%d hidrogenio\n %d helio\n %d gravidade\n %d gas\n", hidrogenio, 
    helio, gravidade, gas);

    return 0;
}